const { _, Sequelize, hash, age, parseDuration, createError, DateTime, fs, persistModel, compressing } = require('rappopo-sob').Helper
const path = require('path')

const schema = {
  siteId: Sequelize.INTEGER,
  userId: Sequelize.INTEGER,
  code: Sequelize.STRING(50),
  type: Sequelize.STRING(20),
  note: Sequelize.STRING,
  output: Sequelize.STRING,
  downloaded: { type: Sequelize.INTEGER, defaultValue: 0 },
  status: { type: Sequelize.STRING(10), defaultValue: 'INQUEUE' },
  payload: Sequelize.TEXT
}

const fields = _.concat(
  ['id', 'createdAt', 'updatedAt'],
  _.keys(schema)
)

const entityValidator = {
  type: { type: 'string', empty: false, trim: true },
  note: { type: 'string', empty: false, trim: true },
  payload: { type: 'string', empty: false, trim: true }
}

module.exports = ({ dbSql, sob }) => {
  const throttle = parseDuration(sob.config.throttleNewJob, 's')
  return {
    mixins: [dbSql],
    model: {
      define: schema,
      options: {
        underscored: true,
        indexes: [
          { fields: ['type'] },
          { fields: ['status'] },
          { fields: ['code'] },
          { fields: ['site_id'] },
          { fields: ['user_id'] },
          { fields: ['output'] },
          { fields: ['updated_at'] }
        ]
      }
    },
    settings: {
      fields,
      entityValidator,
      cron: [{
        time: '*/6 * * * * *',
        timeout: '5m',
        autoStart: true,
        runOnAutoStart: true,
        handler: 'processDbexport'
      }]
    },
    hooks: {
      before: {
        create: ['beforeCreate'],
        apiCreate: ['beforeCreate']
      }
    },
    actions: {
      async processDbexport () {
        const start = DateTime.local()
        let job = await this.broker.call('downloadJob.find', {
          query: { status: 'INQUEUE', type: 'DBEXPORT' },
          limit: 1,
          sort: ['-id']
        })
        if (job.length === 0) return false
        job = job[0]
        await this.broker.call('downloadJob.update', { id: job.id, status: 'PROCESSING' })
        const dir = path.join(this.broker.sobr.dataDir, 'tmp', 'DBEXPORT')
        fs.ensureDirSync(dir)
        const payload = JSON.parse(job.payload)
        // const file = hash(`${job.code}:${job.id}`) + '.' + payload.format
        let format = 'jsonl'
        if (['json'].includes(payload.format)) format = payload.format
        const file = hash(`${job.code}:${job.id}`) + '.' + format
        let dest = path.join(dir, file)
        const svc = this.broker.getLocalService(payload.entity)
        if (!svc) throw createError(`Invalid entity '${payload.entity}'`)
        const model = svc.adapter.model
        if (!model) throw createError(`Can't find model for entity '${payload.entity}'`)
        const maxBatchSize = _.get(this.broker.sobr, 'sob.core.config.web.stream.maxBatchSize', 1000)
        const maxRows = _.get(this.broker.sobr, 'sob.core.config.web.stream.maxRows', 100000)
        await persistModel(model, dest, { batchSize: maxBatchSize, limit: maxRows, format })
        if (payload.compressed) {
          const basename = path.basename(dest)
          const newDest = path.join(dir, basename + '.gz')
          await compressing.gzip.compressFile(dest, newDest)
          await fs.unlink(dest)
          dest = newDest
        }
        job = await this.broker.call('downloadJob.update', { id: job.id, status: 'COMPLETE', output: path.basename(dest) })
        this.broker.broadcast('priv:downloadJob', _.omit(job, ['payload']), ['streamer'])
        return `Job '${job.id}', type '${job.type}', entity '${payload.entity}' is completed in ${age(start)}`
      }
    },
    methods: {
      async beforeCreate (ctx) {
        const code = hash(`${ctx.params.type}:${ctx.params.note}`)
        ctx.params.code = code
        const existing = await this.broker.call('downloadJob.find', { query: { code, siteId: ctx.meta.site.id, userId: ctx.meta.user.id, status: 'INQUEUE' }, limit: 1, sort: ['-id'] })
        if (existing.length > 0) {
          const elapsed = age(DateTime.fromJSDate(existing[0].createdAt), null, false) // in seconds
          if (elapsed < throttle) throw createError('New job throttled. Please wait until threshold passed')
        }
      }
    }
  }
}
