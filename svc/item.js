const { _, createError, fs, DateTime, parseDuration, age, mime } = require('rappopo-sob').Helper
const path = require('path')

module.exports = ({ sobr, sob }) => {
  return {
    settings: {
      webRoot: {
        alias: {
          'GET /:file': 'serve'
        }
      },
      cron: {
        time: '* * * * *',
        timeout: '5m',
        autoStart: true,
        runOnAutoStart: true,
        handler: 'purge'
      }
    },
    actions: {
      async serve (ctx) {
        if (_.isEmpty(ctx.params.file)) throw createError('File not provided', 500)
        let job = await this.broker.call('downloadJob.find', { query: { output: ctx.params.file }, limit: 1 })
        if (job.length === 0) throw createError('Job record not found', 404)
        job = job[0]
        const payload = JSON.parse(job.payload)
        const file = path.join(sobr.dataDir, 'tmp', 'DBEXPORT', ctx.params.file)
        if (!fs.existsSync(file)) throw createError('File not found or download is expired')
        await this.broker.call('downloadJob.update', { id: job.id, downloaded: job.downloaded + 1 })
        // ctx.meta.$responseType = 'application/x-ndjson'
        const type = mime.lookup(path.extname(file))
        if (type) ctx.meta.$responseType = type
        const [base, ...exts] = path.basename(file).split('.') // eslint-disable-line
        ctx.meta.$responseHeaders = {
          'Content-Disposition': `attachment; filename=${_.snakeCase(payload.entity)}.${exts.join('.')}`
        }
        return fs.createReadStream(file)
      },
      async purge (ctx) {
        const start = DateTime.local()
        const milliseconds = parseDuration(sob.config.expireAfter)
        const dt = DateTime.utc().minus({ milliseconds }).toJSDate()
        const result = await this.broker.call('downloadJob.find', { query: { createdAt: { $lt: dt } } })
        if (result.length === 0) return false
        await this.broker.call('downloadJob.modelDestroy', { where: { id: { $in: _.map(result, 'id') } } })
        for (let i = 0; i < result.length; i++) {
          const file = path.join(sobr.dataDir, 'tmp', result[i].type, result[i].output)
          try {
            fs.unlinkSync(file)
          } catch (err) {}
        }
        return `purge ${result.length} old downloads in ${age(start)}`
      }
    }
  }
}
